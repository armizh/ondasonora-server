$:.unshift File.join(__dir__, 'lib')

require 'json'
require 'date'
require 'boolean'
require 'firebase'

mode = ARGV.include? 'production' ? :production : :development
file = File.read 'config/firebase.json'
data = JSON.parse file, :symbolize_keys => true

Firebase.config data[mode]

require_relative 'models'

admin_token = Firebase[:api_tokens].create({
  name: 'admin',
  role: 'admin'
})

p admin_token

client_token = Firebase[:api_tokens].create({
  name: 'onda-client',
  role: 'client'
})

p client_token