require 'indifferent-hash'
require 'altair'
require 'firebase/field'
require 'firebase/rest'
require 'firebase/query'
require 'firebase/response'
require 'firebase/document'
require 'firebase/schema'
require 'firebase/document_not_found'

module Firebase
  attr_accessor :query, :models

  def config(options = {})
    self.query = Query.new(options[:project_id])
    self.models = IndifferentHash.new

    Altair.config(options)
  end

  def register_model(schema)
    self.models[schema.key] = schema
  end

  def [](name)
    self.models[name]
  end

  extend self
end