require 'uri'
require 'json'
require 'firebase/rest'

module Firebase
  class Query
    attr_accessor :project_id, :endpoint, :route, :options
  
    def initialize(project_id, route = [], options = {})
      self.project_id = project_id
      self.options = options
      self.endpoint = Rest.new(project_id)
      self.route = Array.new(route)
    end

    def [](name)
      dest = Query.new(project_id, route, options)
      dest.append_route name
      dest
    end

    def append_route(*args)
      args.each { |arg| route << arg }
    end

    def get
      endpoint.get route
    end

    def put(data)
      endpoint.put route, data
    end

    def post(data)
      endpoint.post route, data
    end

    def patch(data)
      endpoint.patch route, data
    end

    def delete
      endpoint.delete route
    end
  end
end