require 'altair'
require 'firebase/document'
require 'firebase/query'
require 'firebase/document_not_found'
require 'firebase/field'

module Firebase
  class Schema < Altair::Schema
    attr_accessor :key

    def initialize(key, fields)
      parse_field(fields, self)

      @key = key
      @hooks = {
        create: [],
        save: [],
        saved: [],
        created: []
      }
    end

    def parse_field(schema, result = IndifferentHash.new)
      case schema
      when Hash
        if schema.has_key? :type
          Field.new(schema)
        else
          schema.each do |key, field|
            result[key] = parse_field(field)
          end

          result
        end
      when Array
        Field.new(type: parse_field(schema.first), array: true, default: [])
      else
        Field.new(type: schema)
      end
    end

    def get(key)
      doc = Firebase::Document.new self
      
      response = query[key].get
      
      raise DocumentNotFound.new(self, key) if response.body.nil?

      doc.apply response.body
      doc.key = key
      
      doc
    end

    def all
      data = query.get.body || {}

      data.map do |key, value|
        doc = Firebase::Document.new self

        doc.apply value
        doc.key = key
        
        doc
      end
    end

    def select(&block)
      all.select(&block)
    end

    def query
      Firebase.query[key]
    end

    def create(data)
      doc = Firebase::Document.new self
      
      doc.create
      doc.apply IndifferentHash.from_hash(data)

      call_hooks(:create, doc)
      call_hooks(:save, doc)

      doc.key = query.post(doc).body['name']

      call_hooks(:saved, doc)
      call_hooks(:created, doc)

      doc
    end

    def save(doc)
      call_hooks(:save, doc)

      query[doc.key].patch(doc)

      call_hooks(:saved, doc)
    end

    def delete(doc)
      query[doc.key].delete
    end

    def add_hook(name, &block)
      if [:create, :save, :saved, :created].include? name.to_sym
        @hooks[name] << block
      else

      end
    end

    def call_hooks(name, target)
      @hooks[name].each { |hook| hook.call(target) }
    end
  end
end