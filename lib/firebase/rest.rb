require 'rest-client'
require 'firebase/response'

module Firebase
  class Rest
    attr_reader :project_id, :base_url

    def initialize(project_id)
      @project_id = project_id
      @base_url = "https://#{project_id}.firebaseio.com"
    end
    
    def get_uri(route)
      URI.join(base_url, route.join('/') + '.json')
    end

    def get(route)
      Response.new(RestClient.get(get_uri(route).to_s))
    end

    def put(route, data)
      Response.new(RestClient.put(get_uri(route).to_s, data.to_json))
    end

    def post(route, data)
      Response.new(RestClient.post(get_uri(route).to_s, data.to_json))
    end

    def patch(route, data)
      Response.new(RestClient.patch(get_uri(route).to_s, data.to_json))
    end

    def delete(route)
      Response.new(RestClient.delete(get_uri(route).to_s))
    end
  end
end