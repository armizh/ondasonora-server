require 'altair'

module Firebase
  class Field < Altair::Field
    def initialize(options)
      options[:required] = options[:required] || !options[:optional]
      
      super(options)
    end
    
    def parse! (value, args = {})
      if options[:array]
        case type
        when Hash
          value.map do |v|
            result = Document.new(type)
            result.apply(v)
            result
          end
        else
          value.each { |v| type.parse!(v, args) }
        end
      else
        super(value, args)
      end
    end

    def validate! (value, args = {})
      if options[:array]
        value.map { |v| type.validate!(v, args) }
      else
        super(value, args)
      end
    end
  end
end