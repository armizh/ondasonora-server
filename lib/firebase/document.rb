require 'altair/document'

module Firebase
  class Document < Altair::Document
    attr_accessor :key, :schema

    def initialize(schema, data = IndifferentHash.new)
      super(schema, data)

      @schema = schema
    end

    def create
      schema.each do |key, field|
        case field
        when Hash
          self[key].create
        else
          self[key] = resolve_proc field.options[:default]
        end
      end
    end      

    def save
      schema.save self
    end

    def delete
      schema.delete self
    end
  end
end