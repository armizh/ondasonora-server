require 'json'

module Firebase
  class Response
    attr_reader :headers, :code, :body

    def initialize(data)
      @headers  = data.headers
      @code     = data.code
      @body     = JSON.parse(data.body)
    end
  end
end