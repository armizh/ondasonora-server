module Firebase
  class DocumentNotFound < StandardError
    attr_reader :schema, :key

    def initialize(schema, key)
      @schema = schema
      @key = key
    end

    def message
      "Cannot find document `#{key}`"
    end
  end
end