require 'altair'
require 'altair/schema'

module Sinatra
  module Params
    def validate_params! (schema)
      doc = Altair::Document.new(Altair::Schema.new(schema), params)
      doc.validate!
      doc
    end
  end
end