require 'json'
require 'indifferent-hash'

module OndaSonora
  class Error < StandardError
    attr_reader :name, :message, :status

    def initialize(name, message, status)
      @name = name
      @message = message
      @status = status
    end

    def to_h
      IndifferentHash.from_hash error: {
        name: @name,
        message: @message
      }
    end

    def to_json(options = nil)
      to_h.to_json
    end
  end
end
