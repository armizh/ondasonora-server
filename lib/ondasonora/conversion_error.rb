require 'json'
require 'indifferent-hash'

module Altair
  class ConversionError
    def to_h
      IndifferentHash.from_hash error: {
        name: 'Conversion error',
        type: type,
        value: value,
        message: message
      }
    end

    def to_json(options = nil)
      to_h.to_json
    end
  end
end
