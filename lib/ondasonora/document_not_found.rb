require 'json'
require 'indifferent-hash'

module Firebase
  class DocumentNotFound
    def to_h
      IndifferentHash.from_hash error: {
        name: 'Document not found',
        key: key,
        schema: schema.key,
        message: message
      }
    end

    def to_json(options = nil)
      to_h.to_json
    end
  end
end