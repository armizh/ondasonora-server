require 'ondasonora/error'

module OndaSonora
  class AuthorizationError < OndaSonora::Error
    def initialize(name, message)
      super(name, message, 401)
    end
  end
end