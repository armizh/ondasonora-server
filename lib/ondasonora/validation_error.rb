require 'json'
require 'indifferent-hash'

module Altair
  class ValidationError
    def to_h
      IndifferentHash.from_hash error: {
        name: 'Validation error',
        validator: validator,
        value: value,
        message: message
      }
    end

    def to_json(options = nil)
      to_h.to_json
    end
  end
end