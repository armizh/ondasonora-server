require 'ondasonora/authorization_error'
require 'ondasonora/authentication_error'

module OndaSonora
  def self.InvalidToken
    AuthorizationError.new 'Invalid token', 'API Token is either not preset or not valid'
  end

  def self.NotAuthorized
    AuthorizationError.new 'Not authorized', 'API Token is not authorized to perform this action'
  end

  def self.WrongPassword
    AuthenticationError.new 'Wrong password', 'Password does not match'
  end
  
  def self.NotCreator
    AuthenticationError.new 'Not creator', 'The account is not allowed to create projects'
  end

  def self.CannotActivate
    AuthenticationError.new 'Cannot activate', 'The account is either already active or banned'
  end

  def self.CannotApply
    AuthenticationError.new 'Cannot apply for creator', 'The account is unelligible for this action'
  end
end