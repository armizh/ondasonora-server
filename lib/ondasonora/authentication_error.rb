require 'ondasonora/error'

module OndaSonora
  class AuthenticationError < OndaSonora::Error
    def initialize(name, message)
      super(name, message, 403)
    end
  end
end