require 'altair'
require 'firebase'
require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/params'
require 'ondasonora/api_errors'

module OndaSonora
  class Controller < Sinatra::Base
    helpers Sinatra::Params

    set :show_exceptions, false

    set :auth do |*roles|
      condition do
        token_id = request.env['HTTP_AUTHORIZATION']
        api_token = Firebase[:api_tokens].select { |doc| doc.id == token_id }.first
  
        raise OndaSonora.InvalidToken if api_token.nil?
        raise OndaSonora.NotAuthorized unless roles.include? api_token.role.to_sym
      end
    end

    error OndaSonora::Error do
      e = env['sinatra.error']

      status e.status
      json e
    end
    
    error Altair::ValidationError, Altair::ConversionError do
      status 400
      json env['sinatra.error']
    end

    error Firebase::DocumentNotFound do
      status 404
      json env['sinatra.error']
    end
  end
end