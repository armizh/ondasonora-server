$:.unshift File.join(__dir__, 'lib')

require 'rack/contrib'
require 'date'
require 'boolean'
require 'altair'
require 'firebase'
require 'ondasonora'
require 'basic-password'

Firebase.config project_id: 'onda-dev'

require_relative 'models'
require_relative 'controllers'

use Rack::PostBodyContentTypeParser

map('/users') { run Controllers::User }
map('/sessions') { run Controllers::Session }
map('/projects') { run Controllers::Project }