$:.unshift File.join(__dir__, 'lib')

require 'date'
require 'boolean'
require 'firebase'
require 'altair'
require 'basic-password'

Firebase.config project_id: 'onda-dev'

require_relative 'models'

begin 
  token = Firebase[:api_tokens].create({
    name: 'onda-client',
    role: 'client'
  })

  p token

  token.delete
end

begin
  user = Firebase[:users].create({
    id: '8888888',
    email: 'alexjones@gayfrogs.com',
    name: {
      first: 'Alex',
      last: 'Jones'
    },
    birthday: '1965-01-01',
    password: BasicPassword.digest('turning the frickin frogs gay')
  })

  p user
  p BasicPassword.check('turning the frickin frogs gay', user.password.salt, user.password.hash)

  user.delete
end

begin
  cart = Firebase[:carts].create({
    user_id: user.key,
    items: [{
      project_id: '',
      amount: '10000'
    }]
  })

  p cart

  cart.delete
end