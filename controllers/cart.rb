require 'firebase'
require 'ondasonora'

module Controllers
  class Cart < OndaSonora::Controller
    get '/:cart_id', :auth => :client do |cart_id|
      json Firebase[:carts].get(cart_id)
    end
  end
end