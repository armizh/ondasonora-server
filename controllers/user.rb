require 'firebase'
require 'ondasonora'
require 'basic-password'

module Controllers
  class User < OndaSonora::Controller
    post '/', :auth => :client do
      params = validate_params! ({
        id: Integer,
        email: String,
        name: {
          first: String,
          last: String
        },
        profile: {
          display_name: { type: String, optional: true },
          about: { type: String, optional: true }
        },
        birthday: Date,
      })

      Firebase[:users].create(params)

      status 201
      json transaction: {
        message: 'User created',
        timestamp: DateTime.now
      }
    end

    get '/:user_id', :auth => :client do |user_id|
      json Firebase[:users].get(user_id)
    end

    post '/:user_id/activate', :auth => :client do |user_id|
      params = validate_params! ({
        password: String
      })

      user = Firebase[:users].get(user_id)

      raise OndaSonora.CannotActivate if user.settings.activated || user.settings.banned

      user.password.apply(BasicPassword.digest(params.password))
      user.settings.activated = true
      user.save

      json transaction: {
        key: user.key,
        message: 'Account activated',
        timestamp: DateTime.now
      }
    end

    post '/:user_id/apply_for_creator', :auth => :client do |user_id|
      user = Firebase[:users].get(user_id)
      applications = Firebase[:creator_appliances].select { |doc| doc.user_id == user.key }

      raise OndaSonora.CannotApply if user.settings.creator || user.settings.banned || applications.length > 0

      application = Firebase[:creator_appliances].create(user_id: user.key)

      status 202
      json transaction: {
        key: application.key,
        message: 'Application received',
        timestamp: DateTime.now
      }
    end

    get '/:user_id/projects', :auth => :client do |user_id|
      user = Firebase[:users].get(user_id)
      list = Firebase[:projects].select { |doc| doc[:user_id] == user.key }

      json list
    end

    patch '/:user_id/profile', :auth => :client do |user_id|
      params = validate_params! ({
        session_id: String,
        display_name: String,
        about: String
      })

      session = Firebase[:sessions].get(params.session_id)
      user = Firebase[:users].get(user_id)

      user.profile.display_name = params.display_name
      user.profile.about = params.about

      user.save

      status 201
      json transaction: {
        key: user.key,
        message: "User's profile updated",
        timestamp: DateTime.now
      }
    end
  end
end