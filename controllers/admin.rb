require 'firebase'
require 'ondasonora'

module Controllers
  class Admin < OndaSonora::Controller
    get '/appliances/:application_id', :auth => :admin do |application_id|
      json Firebase[:creator_appliances].get(application_id)
    end

    post '/appliances/:application_id/accept', :auth => :admin do |application_id|
      params = validate_params! ({
        comment: { type: String, optional: true }
      })

      application = Firebase[:creator_appliances].get(application_id)
      application.status = 'accepted'
      application.comment = params.comment
      application.save

      user = Firebase[:users].get(application.user_id)
      user.settings.creator = true
      user.save

      status 202
      json transaction: {
        key: application.key,
        message: 'Application accepted',
        timestamp: DateTime.now
      }
    end

    post '/appliances/:application_id/reject', :auth => :admin do |application_id|
      params = validate_params! ({
        comment: { type: String, optional: true }
      })

      application = Firebase[:creator_appliances].get(application_id)
      application.status = 'rejected'
      application.comment = params.comment
      application.save

      status 202
      json transaction: {
        key: application.key,
        message: 'Application rejected',
        timestamp: DateTime.now
      }
    end
  end
end