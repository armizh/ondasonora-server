require 'firebase'
require 'ondasonora'
require 'basic-password'
require 'utils'

module Controllers
  class Session < OndaSonora::Controller
    post '/', :auth => :client do
      params = validate_params! ({
        email: String,
        password: String
      })

      list = Firebase[:users].select { |doc| doc[:email] == params.email }
      user = list.first

      raise OndaSonora::WrongPassword unless BasicPassword.check(params.password, user.password.salt, user.password.hash)
        
      session = Firebase[:sessions].create(user_id: user.key)

      status 201
      json transaction: {
        key: session.key,
        message: 'Session created',
        timestamp: DateTime.now
      }
    end

    get '/:session_id', :auth => :client do |session_id|
      session = Firebase[:sessions].get(session_id)
      session.save

      json session
    end

    post '/:session_id/close', :auth => :client do |session_id|
      session = Firebase[:sessions].get(session_id)
      session.active = false
      session.save

      json transaction: {
        key: session.key,
        message: 'Session closed',
        timestamp: DateTime.now
      }
    end
  end
end