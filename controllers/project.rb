require 'firebase'
require 'ondasonora'

module Controllers
  class Project < OndaSonora::Controller
    post '/', :auth => :client do
      params = validate_params! ({
        session_id: String
      })

      session = Firebase[:sessions].get(params.session_id)

      params = validate_params! ({
        user_id: { type: String, optional: true },
        name: String,
        description: String,
        deadline: Date,
        goal: Integer
      })

      params.user_id = session.user_id

      user = Firebase[:users].get(params.user_id)

      raise OndaSonora.NotCreator unless user.settings.creator
      
      project = Firebase[:projects].create(params)

      status 201
      json transaction: {
        key: project.key,
        message: 'Project created',
        timestamp: DateTime.now
      }
    end

    get '/:project_id', :auth => :client do |project_id|
      json Firebase[:projects].get(project_id)
    end
  end
end