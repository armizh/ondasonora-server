require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'media', {
  user_id: String,
  path: String,
  kind: { type: String, in: ['photo', 'video', 'audio'] },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema