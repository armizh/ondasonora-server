require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'users', {
  id: { type: Integer, unique: true },
  email: { type: String, unique: true },
  name: {
    first: String,
    last: String
  },
  profile: {
    display_name: String,
    about: { type: String, optional: true }
  },
  birthday: { type: Date, max: ->{ Date.today.prev_year(18) } },
  password: {
    salt: String,
    hash: String
  },
  settings: {
    activated: { type: Boolean, default: false },
    creator: { type: Boolean, default: false },
    banned: { type: Boolean, default: false }
  },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :create do |doc|
  doc.profile.display_name = doc.name.values.join(' ')
end

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema