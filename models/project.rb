require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'projects', {
  user_id: String,
  name: String,
  description: String,
  media: [String],
  tiers: [{
    name: String,
    about: String,
    floor: Integer
  }],
  deadline: { type: Date, min: ->{ Date.today.next_month } },
  goal: { type: Integer, min: 10000 },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema