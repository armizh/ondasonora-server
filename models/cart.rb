require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'carts', {
  user_id: String,
  items: [{
    project_id: String,
    amount: Integer
  }],
  settings: {
    status: { type: String, in: ['processing', 'paid', 'shopping'] }
  },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema