require 'date'
require 'digest'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'api_tokens', {
  id: { type: String, default: ->{ Digest::SHA2.new(512).base64digest(DateTime.now.to_time.to_s) } },
  name: String,
  role: { type: String, in: ['admin', 'client', 'read_only'] }
}

Firebase.register_model schema