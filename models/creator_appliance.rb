require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'creator_appliances', {
  user_id: String,
  status: { type: String, in: ['waiting', 'accepted', 'rejected'], default: 'waiting' },
  comment: { type: String, optional: true },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema