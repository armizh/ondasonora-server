require 'date'
require 'boolean'
require 'firebase'

schema = Firebase::Schema.new 'sessions', {
  user_id: String,
  connections: [{
    ip: String,
    browser: String,
    timestamp: DateTime
  }],
  active: { type: Boolean, default: true },
  updated_at: DateTime,
  created_at: { type: DateTime, default: ->{ DateTime.now } }
}

schema.add_hook :save do |doc|
  doc.updated_at = DateTime.now
end

Firebase.register_model schema